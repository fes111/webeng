const cvs = document.getElementById('canvas');

const start = document.getElementById('start');
const stop = document.getElementById('stop');
const punkteLbl = document.getElementById('punkte');

const ctx = cvs.getContext('2d');
ctx.strokeStyle = 'white';
ctx.fillStyle = 'white';
ctx.lineWidth = 3;

const startGame = () => {
  punkte = 0;
  ctx.clearRect(0, 0, cvs.width, cvs.height);
  snake = new Snake(ctx);
  snake.draw();

  inteval = setInterval(() => {
    snake.update();
    snake.draw();
    punkte++;
    punkteLbl.innerText = punkte;
  }, 200);

  snake.onCrash(() => {
    clearInterval(interval);
    alert('crashed');
    startGame();
  });
};
let snake, interval, punkte;

start.addEventListener('click', startGame);

document.addEventListener('keyup', (e) => {
  if (e.code === 'ArrowUp') snake.move(0, -1);
  else if (e.code === 'ArrowDown') snake.move(0, 1);
  else if (e.code === 'ArrowLeft') snake.move(-1, 0);
  else if (e.code === 'ArrowRight') snake.move(1, 0);
});
