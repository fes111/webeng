class Snake {
  constructor(ctx) {
    this.ctx = ctx;
    this.x = 0;
    this.y = 0;
    this.xS = 1;
    this.yS = 0;
  }

  draw() {
    this.ctx.fillRect(this.x, this.y, 10, 10);
  }

  move(xS, yS) {
    this.xS = xS;
    this.yS = yS;
  }

  onCrash(callback) {
    this.cb = callback;
  }

  update() {
    this.x = this.x + this.xS * 10;
    this.y = this.y + this.yS * 10;

    if (this.x < 0 || this.x > 600) {
      this.cb();
      return;
    }
    if (this.y < 0 || this.y > 600) {
      this.cb();
      return;
    }
    console.log();
    if (this.ctx.getImageData(this.x, this.y, 1, 1).data[0] == 255) {
      this.cb();
      return;
    }
  }
}
