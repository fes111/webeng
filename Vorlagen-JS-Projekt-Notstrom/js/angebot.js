'use strict';

// Es sind nicht alle Übungen

const preis = document.getElementById('link');
const preisbereich = document.getElementById('preisbereich');
const calcButton = document.getElementById('calcPreis');
const modal = document.querySelector('.modal');
const modalClose = document.querySelector('.close');
const modalYes = document.querySelector('.btn-yes');
const modalNo = document.querySelector('.btn-no');

const img = document.querySelector('#bildbereich').childNodes[1];
const imgKreis = document.querySelector('#bildbereich').childNodes[3];
const lupe = document.getElementById('lupe');
let brutto = true;

const nur = preisbereich.childNodes[0];
const preisLabel = preisbereich.childNodes[2];
const stuerLabel = preisbereich.childNodes[3];

calcButton.addEventListener('click', () => {
  let preis = preisLabel.textContent;

  if (brutto) {
    preis = (preis / 1.19).toFixed(2);
    preisLabel.textContent = preis;
    stuerLabel.textContent = ' Euro exkl. 19% MwSt. ';
    calcButton.textContent = 'zeige Bruttopreis';
    brutto = false;
  } else {
    preis = (preis * 1.19).toFixed(2);
    preisLabel.textContent = preis;
    stuerLabel.textContent = ' Euro inkl. 19% MwSt. ';
    calcButton.textContent = 'zeige Nettopreis';
    brutto = true;
  }
});

setTimeout(() => {
  showModal();
}, 10 * 1000);

const showModal = () => {
  modal.classList.toggle('hidden');
  modal.style.animation = 'modalAn 0.3s ease';
};

const hideModal = () => {
  modal.classList.toggle('hidden');
};

modalClose.addEventListener('click', hideModal);
modalNo.addEventListener('click', hideModal);

modalYes.addEventListener('click', () => {
  let fenster = window.open('https://neilpatel.com/wp-content/uploads/2017/04/chat.jpg', 'Chat', 'width=600,height=400,status=no,scrollbars=no,resizable=no');
  fenster.focus();

  hideModal();
});

lupe.addEventListener('click', toggleKreis);

img.addEventListener('click', () => {
  toggleImg();
});

img.addEventListener('mouseover', () => {
  toggleImg();
});

img.addEventListener('mouseout', () => {
  toggleImg();
});

function toggleKreis() {
  imgKreis.classList.toggle('hidden');
}

function toggleImg() {
  if (img.getAttribute('src') === 'img/notstromaggregat.jpg') {
    img.setAttribute('src', 'img/notstromaggregat-rueckseite.jpg');
  } else {
    img.setAttribute('src', 'img/notstromaggregat.jpg');
  }
}

preis.addEventListener('click', () => {
  nur.style.color = 'green';
  preisLabel.style.color = 'red';
});
