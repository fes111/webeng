module.exports = {
  jsxSingleQuote: true,
  singleQuote: true,
  printWidth: 240,
  tabWidth: 2,
  trailingComma: 'none',
  semi: true
};
